﻿namespace DungeonHero.World.Abstract
{
    public abstract class Terrain : Field
    {
        protected Terrain() : base(true)
        {
        }
    }
}
