﻿using DungeonHero.World.Abstract;
using DungeonHero.World.Common;

namespace DungeonHero.World.Grassland
{
    public class GrasslandGenerator : AbstractWorldGenerator
    {
        public override Plant CreatePlant()
        {
            return new HighGrass();
        }

        public override Terrain CreateTerrain()
        {
            return new Grass();
        }
    }
}
