﻿using DungeonHero.Game;
using DungeonHero.World.Abstract;
using DungeonHero.World.Grassland;
using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonHero.World
{
    public class Map
    {
        private readonly Field[,] fields;
        private readonly Dictionary<IMoveable, Position> moveableObjects = new Dictionary<IMoveable, Position>();

        public Map(Field[,] fields)
        {
            this.fields = fields;

        }

        public Map(AbstractWorldGenerator generator, Position mapSize)
        {
            fields = generator.GenerateRoom((uint)mapSize.X, (uint)mapSize.Y);
        }

        public void Place(IMoveable obj, Position position)
        {
            moveableObjects.Add(obj, position);
        }

        public Position GetPosition(IMoveable obj)
        {
            if (moveableObjects.ContainsKey(obj))
            {
                return moveableObjects[obj];
            }
            return Position.Invalid;
        }

        public Position Move(IMoveable obj, Position relativeMovement)
        {
            if (moveableObjects.ContainsKey(obj))
            {
                var position = moveableObjects[obj];
                position.X += relativeMovement.X;
                position.Y += relativeMovement.Y;

                if (fields[position.X, position.Y].CanBeEntered)
                {
                    moveableObjects[obj] = position;
                    return position;
                }
                return Position.Invalid;
            }
            return Position.Invalid;
        }
    }
}
