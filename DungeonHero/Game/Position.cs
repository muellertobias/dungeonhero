﻿using System;

namespace DungeonHero.Game
{
    public class Position
    {
        public int X { get; set; }
        public int Y { get; set; }

        // override object.Equals
        public override bool Equals(object obj)
        {
            //       
            // See the full list of guidelines at
            //   http://go.microsoft.com/fwlink/?LinkID=85237  
            // and also the guidance for operator== at
            //   http://go.microsoft.com/fwlink/?LinkId=85238
            //

            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            var p = (Position)obj;
            if (p.X == this.X && p.Y == this.Y)
            {
                return true;
            }

            return base.Equals(obj);
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            // TODO: write your implementation of GetHashCode() here
            return base.GetHashCode();
        }

        public static Position Invalid { get; } = new Position();
    }
}
