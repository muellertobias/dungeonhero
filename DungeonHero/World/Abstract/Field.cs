﻿namespace DungeonHero.World.Abstract
{
    public abstract class Field
    {
        public bool CanBeEntered { get; }
        public Field(bool canBeEntered)
        {
            CanBeEntered = canBeEntered;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            if (this == obj)
                return true;
            if (this.GetType() == obj.GetType())
                return true;
            return false;
        }
    }
}
