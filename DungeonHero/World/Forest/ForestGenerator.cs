﻿using DungeonHero.World.Abstract;
using DungeonHero.World.Common;

namespace DungeonHero.World.Forest
{
    public class ForestGenerator : AbstractWorldGenerator
    {
        public override Plant CreatePlant()
        {
            return new Tree();
        }

        public override Terrain CreateTerrain()
        {
            return new Leaves();
        }
    }
}
