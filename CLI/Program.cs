﻿using DungeonHero.Game;
using System;
using System.IO;

namespace CLI
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleInput input = new ConsoleInput(Console.In, Console.Out);
            Game game = new Game(input, new Position{X = 10, Y = 20 });

            while (true)
            {
                game.Run();
            }
        }
    }

    class ConsoleInput : IInteractPlayer
    {
        private readonly TextReader input;
        private readonly TextWriter output;

        public ConsoleInput(TextReader input, TextWriter output)
        {
            this.input = input ?? throw new ArgumentNullException(nameof(input));
            this.output = output ?? throw new ArgumentNullException(nameof(output));
        }

        public void Blocked()
        {
            output.WriteLine("Blocked!");
        }

        public Position GetPlayerMovement()
        {
            output.Write("Wohin möchtest du gehen? (North, South, West, East)\n> ");

            string x = input.ReadLine();
            Position p = new Position();

            if (x.ToUpper() == "NORTH")
            {
                p.X = 0;
                p.Y = -1;
            }
            else if (x.ToUpper() == "SOUTH")
            {
                p.X = 0;
                p.Y = 1;
            }
            else if (x.ToUpper() == "WEST")
            {
                p.X = -1;
                p.Y = 0;
            }
            else if (x.ToUpper() == "EAST")
            {
                p.X = 1;
                p.Y = 0;
            }
            

            if (p.Equals(new Position()))
            {
                output.WriteLine("Hä?!");
            }


            return p;
        }

        public void GetPosition(Position newPosition)
        {
            output.WriteLine($"Current Player Position: {newPosition.X}/{newPosition.Y}");
        }
    }
}
