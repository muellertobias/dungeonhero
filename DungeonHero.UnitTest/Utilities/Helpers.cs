﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonHero.UnitTest.Utilities
{
    public static class Helpers
    {
        public static void AreFieldsEquals<T>(T[,] expected, T[,] actual)
        {
            Assert.AreEqual(expected.GetLength(0), actual.GetLength(0));
            Assert.AreEqual(expected.GetLength(1), actual.GetLength(1));
            Assert.AreEqual(expected.Rank, actual.Rank);

            for (uint x = 0; x < expected.GetLength(0); x++)
            {
                for (uint y = 0; y < expected.GetLength(1); y++)
                {
                    Assert.AreEqual(expected[x, y], actual[x, y]);
                }
            }
        }
    }
}
