﻿using DungeonHero.World;
using DungeonHero.World.Grassland;
using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonHero.Game
{
    public class Game
    {
        private readonly IInteractPlayer interaction;

        public Player Player { get; }
        public Map Map { get; }

        public Game(IInteractPlayer movementHandler, Position mapSize)
        {
            Player = new Player();
            Map = new Map(new GrasslandGenerator(), mapSize);
            this.interaction = movementHandler ?? throw new ArgumentNullException(nameof(movementHandler));
            Map.Place(Player, new Position { X = 1, Y = 1 });
        }

        public void Run()
        {
            interaction.GetPosition(Map.GetPosition(Player));
            var newRelativePosition = interaction.GetPlayerMovement();
            var newPosition = Map.Move(Player, newRelativePosition);

            if (newPosition.Equals(Position.Invalid))
            {
                interaction.Blocked();
            }
            else
            {

            }
        }
    }
}
