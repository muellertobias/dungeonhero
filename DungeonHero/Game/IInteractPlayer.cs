﻿namespace DungeonHero.Game
{
    public interface IInteractPlayer
    {
        Position GetPlayerMovement();
        void GetPosition(Position newPosition);
        void Blocked();
    }
}
