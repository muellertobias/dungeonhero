﻿namespace DungeonHero.Game
{
    public class Player : IMoveable
    {
        public uint MaxLife { get; private set; }
        public uint CurrentLife { get; private set; }

        public Player() 
        {
            MaxLife = 100;
            CurrentLife = MaxLife;
        }
    }
}
