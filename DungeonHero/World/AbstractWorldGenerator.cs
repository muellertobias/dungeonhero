﻿using DungeonHero.World.Abstract;

namespace DungeonHero.World
{
    public abstract class AbstractWorldGenerator
    {
        public abstract Plant CreatePlant();
        public abstract Terrain CreateTerrain();

        public Map Create(uint sizeX, uint sizeY)
        {
            var fields = new Field[sizeX, sizeY];
            return new Map(fields);
        }

        public Field[,] GenerateRoom(uint sizeX, uint sizeY)
        {
            var room = new Field[sizeX, sizeY];

            for (uint x = 0; x < sizeX; x++)
            {
                for (uint y = 0; y < sizeY; y++)
                {
                    if (x == 0 || y == 0 || x == (sizeX - 1) || y == (sizeY - 1))
                    {
                        room[x, y] = CreatePlant();
                    }
                    else
                    {
                        room[x, y] = CreateTerrain();
                    }
                }
            }

            return room;
        }
    }
}
