﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonHero.Utilities
{
    public static class MatrixOperations
    {
        public static T[,] Transpose<T>(this T[,] matrix)
        {
            int sizeX = matrix.GetLength(0);
            int sizeY = matrix.GetLength(1);

            T[,] transposed = new T[sizeY, sizeX];

            for (int x = 0; x < sizeX; x++)
            {
                for (int y = 0; y < sizeY; y++)
                {
                    transposed[y, x] = matrix[x, y];
                }
            }

            return transposed;
        }
    }
}
