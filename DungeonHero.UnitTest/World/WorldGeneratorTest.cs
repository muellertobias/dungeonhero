﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DungeonHero.World.Grassland;
using DungeonHero.World.Abstract;
using DungeonHero.World.Common;
using DungeonHero.Utilities;
using DungeonHero.UnitTest.Utilities;

namespace DungeonHero.UnitTest.World
{
    [TestClass]
    public class WorldGeneratorTest
    {
        [TestMethod]
        public void CreateRoomTest_3x3()
        {
            var generator = new GrasslandGenerator();

            var expectedRoom = new Field[,]
            {
                { generator.CreatePlant(), generator.CreatePlant(),generator.CreatePlant() },
                { generator.CreatePlant(), generator.CreateTerrain(),generator.CreatePlant() },
                { generator.CreatePlant(), generator.CreatePlant(),generator.CreatePlant() }
            }.Transpose();
            
            var room = generator.GenerateRoom(3, 3);

            Helpers.AreFieldsEquals(expectedRoom, room);
        }

        [TestMethod]
        public void CreateRoomTest_5x5()
        {
            var generator = new GrasslandGenerator();

            var expectedRoom = new Field[,]
            {
                { generator.CreatePlant(), generator.CreatePlant(), generator.CreatePlant(), generator.CreatePlant(), generator.CreatePlant() },
                { generator.CreatePlant(), generator.CreateTerrain(), generator.CreateTerrain(), generator.CreateTerrain(), generator.CreatePlant() },
                { generator.CreatePlant(), generator.CreateTerrain(), generator.CreateTerrain(), generator.CreateTerrain(), generator.CreatePlant() },
                { generator.CreatePlant(), generator.CreateTerrain(), generator.CreateTerrain(), generator.CreateTerrain(), generator.CreatePlant() },
                { generator.CreatePlant(), generator.CreatePlant(), generator.CreatePlant(), generator.CreatePlant(), generator.CreatePlant() },
            }.Transpose();

            var room = generator.GenerateRoom(5, 5);

            Helpers.AreFieldsEquals(expectedRoom, room);
        }

        [TestMethod]
        public void CreateRoomTest_5x3()
        {
            var generator = new GrasslandGenerator();

            var expectedRoom = new Field[,]
            {
                { generator.CreatePlant(), generator.CreatePlant(), generator.CreatePlant(), generator.CreatePlant(), generator.CreatePlant() },
                { generator.CreatePlant(), generator.CreateTerrain(), generator.CreateTerrain(), generator.CreateTerrain(), generator.CreatePlant() },
                { generator.CreatePlant(), generator.CreatePlant(), generator.CreatePlant(), generator.CreatePlant(), generator.CreatePlant() },
            }.Transpose();

            var room = generator.GenerateRoom(5, 3);

            Helpers.AreFieldsEquals(expectedRoom, room);
        }
    }
}
